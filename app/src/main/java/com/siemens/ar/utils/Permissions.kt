package com.siemens.ar.utils

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.content.ContextCompat
import com.siemens.ar.ui.MainActivity


const val PERMISSION_CAMERA = 100


fun MainActivity.isCameraPermissionGranted() : Boolean =  isPermissionsNeeded() && ContextCompat
        .checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED

fun isPermissionsNeeded() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
