package com.siemens.ar.data.socket

import com.siemens.ar.data.model.SensorInfo
import com.siemens.ar.data.model.Tag
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.*
import java.util.concurrent.TimeUnit

class MockSensorSocket : SensorSocket() {

    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun start() {
        val client = OkHttpClient.Builder()
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .build()
        val request = Request.Builder()
                .url("ws://echo.websocket.org")
                .build()
        client.newWebSocket(request, this)
        client.dispatcher().executorService().shutdown()
    }

    override fun connectTag(tag: Tag) {
        val disposable = Observable.interval(100, TimeUnit.MILLISECONDS)
                .timeInterval()
                .observeOn(Schedulers.io())
                .subscribe {
                    socket?.send(gson.toJson(SensorInfo((0..999).random().toFloat() / 1000, tag.id)))
                }
        compositeDisposable.add(disposable)
    }

    override fun stop() {
        compositeDisposable.dispose()
    }


    override fun stopTagListening(tag: Tag) {

    }

    fun ClosedRange<Int>.random() =
            Random().nextInt((endInclusive + 1) - start) + start
}
