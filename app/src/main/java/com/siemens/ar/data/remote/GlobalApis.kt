package com.siemens.ar.data.remote

val arApi by lazy {
    ARApi.create()
}