package com.siemens.ar.data

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import com.siemens.ar.computerVision.CvService
import com.siemens.ar.data.model.*
import com.siemens.ar.data.remote.ARApi
import com.siemens.ar.data.socket.MockSensorSocket
import com.siemens.ar.data.socket.SensorSocket
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.opencv.android.Utils
import org.opencv.core.Mat
import org.opencv.core.Rect
import org.opencv.imgproc.Imgproc
import java.io.ByteArrayOutputStream

class RecognitionService : CvService {

    private val arApi = ARApi.create()
    private val sensorSocket: SensorSocket = MockSensorSocket()


    init {
        sensorSocket.start()
    }

    override fun getTag(mat: Mat, rect: Rect): Single<String> {
        return Single.create<TagRequest> {
            val matToSend = Mat(mat, rect)
            val matToSendColored = Mat()
            Imgproc.cvtColor(matToSend, matToSendColored, Imgproc.COLOR_BGR2RGBA)
            val resultBitmap = Bitmap.createBitmap(matToSendColored.cols(), matToSendColored.rows(), Bitmap.Config.ARGB_8888)
            Utils.matToBitmap(matToSendColored, resultBitmap)

            val listImage: MutableList<Request> = mutableListOf()
            val listFeatures: MutableList<Feature> = mutableListOf()
            val feature = Feature("TEXT_DETECTION")
            listFeatures += feature
            val image = Image(ImageUtil.convert(resultBitmap))
            val request = Request(image, listFeatures)
            listImage += request
            it.onSuccess(TagRequest(listImage))
        }.flatMap { arApi.recognizeImage(it) }
                .map { it.responses }
                .map {
                    var description: String = ""
                    it.forEach {
                        it.textAnnotations.forEach {
                            description += it.description
                        }
                    }
                    return@map description
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override fun getSensorInfoFlow(): Observable<SensorInfo> = sensorSocket.listenSensorInfo()

    override fun startTagListening(tag: Tag) {
        sensorSocket.connectTag(tag)
    }
}

object ImageUtil {
    @Throws(IllegalArgumentException::class)
    fun convert(base64Str: String): Bitmap {
        val decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",") + 1),
                Base64.DEFAULT
        )

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
    }

    fun convert(bitmap: Bitmap): String {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
    }

}