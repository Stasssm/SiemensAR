package com.siemens.ar.data.model


data class TagRequest(
    val requests: List<Request>
)

data class Request(
    val image: Image,
    val features: List<Feature>
)

data class Feature(
    val type: String
)

data class Image(
    val content: String
)