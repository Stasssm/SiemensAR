package com.siemens.ar.data.socket

import com.google.gson.Gson
import com.siemens.ar.data.model.SensorInfo
import com.siemens.ar.data.model.Tag
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString

abstract class SensorSocket : WebSocketListener() {

    protected var socket : WebSocket? = null
    protected var behaviorSubject : BehaviorSubject<SensorInfo> = BehaviorSubject.create()
    protected val gson = Gson()
    private var isOpened : Boolean = false

    fun isOpenedConnection() = isOpened

    abstract fun connectTag(tag: Tag)

    abstract fun stopTagListening(tag: Tag)

    abstract fun stop()

    fun listenSensorInfo() : Observable<SensorInfo> = behaviorSubject.hide()

    override fun onOpen(webSocket: WebSocket, response: Response?) {
        socket = webSocket
        isOpened = true
    }

    override fun onMessage(webSocket: WebSocket?, text: String?) {
        behaviorSubject.onNext(gson.fromJson(text, SensorInfo::class.java))
    }

    override fun onMessage(webSocket: WebSocket?, bytes: ByteString?) {
        //  output("Receiving bytes : " + bytes.hex());
    }

    override fun onClosing(webSocket: WebSocket, code: Int, reason: String?) {
        webSocket.close(NORMAL_CLOSURE_STATUS, null)
        // output("Closing : " + code + " / " + reason);
    }

    override fun onFailure(webSocket: WebSocket?, t: Throwable?, response: Response?) {
        //output("Error : " + t.getMessage());
    }

    companion object {

        private val NORMAL_CLOSURE_STATUS = 1000
    }


    abstract fun start()
}
