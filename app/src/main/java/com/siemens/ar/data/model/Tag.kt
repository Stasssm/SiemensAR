package com.siemens.ar.data.model

import java.util.*

data class Tag(
        val id: String = UUID.randomUUID().toString(),
        val text: String
)