package com.siemens.ar.data.model

data class SensorInfo(
        val sensorVal : Float,
        val tagId : String
)
