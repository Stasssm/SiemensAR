package com.siemens.ar.data.remote

import com.siemens.ar.data.model.TagRequest
import com.siemens.ar.data.model.TagResponse
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface ARApi {

    @POST("v1/images:annotate?key=AIzaSyC2rJH7-35EyW6zCmMkhb5qPEEDNwuNfaE")
    fun recognizeImage(@Body tagRequest: TagRequest) : Single<TagResponse>

    companion object {
        fun create(): ARApi {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create())
                    .addConverterFactory(
                            GsonConverterFactory.create())
                    .baseUrl("https://vision.googleapis.com/")
                    .build()
            return retrofit.create(ARApi::class.java)
        }
    }
}