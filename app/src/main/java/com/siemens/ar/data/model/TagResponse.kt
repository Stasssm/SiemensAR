package com.siemens.ar.data.model


data class TagResponse(
    val responses: List<Response>
)

data class Response(
    val textAnnotations: List<TextAnnotation>
)

data class TextAnnotation(
    val locale: String,
    val description: String,
    val boundingPoly: BoundingPoly
)

data class BoundingPoly(
    val vertices: List<Vertice>
)

data class Vertice(
    val x: Int,
    val y: Int
)