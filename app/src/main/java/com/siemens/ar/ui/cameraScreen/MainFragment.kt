package com.siemens.ar.ui.cameraScreen

import android.arch.lifecycle.Observer
import android.graphics.Color
import android.graphics.Point
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.siemens.ar.App
import com.siemens.ar.R
import com.siemens.ar.computerVision.CameraDetectionTracking
import com.siemens.ar.computerVision.TrackerType
import com.siemens.ar.data.RecognitionService
import com.siemens.ar.ui.MainActivity
import kotlinx.android.synthetic.main.main_fragment.*
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.core.Mat
import ru.whalemare.sheetmenu.SheetMenu


class MainFragment : Fragment(), CameraBridgeViewBase.CvCameraViewListener2 {

    private var cameraObserver: CameraDetectionTracking? = null
    private var baseLoaderCallback: BaseLoaderCallback? = null
    private var mat: Mat? = null
    private var fps = 0L;
    private var xToShowText = 0f
    private var yToShowText = 0f
    private var screenWidth = 0


    companion object {
        fun newInstance() = MainFragment()

        private const val WIDTH = 640
        private const val HEIGHT = 480
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val display = activity!!.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        screenWidth = size.x

        val searchWidth = resources.getDimensionPixelSize(R.dimen.search_area_width)
        val searchHeight = resources.getDimensionPixelSize(R.dimen.search_area_height)
        cameraViewer.setMaxFrameSize(WIDTH, HEIGHT);
        cameraViewer.visibility = View.VISIBLE
        cameraViewer.setCvCameraViewListener(this)
        cameraViewer.post {
            val cameraHeight = cameraViewer.height
            val cameraWidth = cameraViewer.width
            cameraObserver = CameraDetectionTracking(cameraWidth, cameraHeight, searchWidth, searchHeight, RecognitionService())
            cameraObserver?.getSensorInfo()?.observe(this, Observer {
                it?.second?.let {
                    Log.d("Show", it.x.toString() + it.y.toString())
                    xToShowText = ((it.x + (it.width / 4)) * screenWidth / WIDTH).toFloat()
                    yToShowText = ((it.y + it.height) * cameraViewer.height / HEIGHT).toFloat()
                    textTagInfo.x = xToShowText
                    textTagInfo.y = yToShowText
                }
                it?.first?.let {
                    textTagInfo.text = it.sensorVal.toString()
                    if (it.sensorVal > 0.5) {
                        textTagInfo.setTextColor(Color.GREEN)
                    } else {
                        textTagInfo.setTextColor(Color.RED)
                    }
                }
            })
        }

        buttonClearAll.setOnClickListener {
            cameraObserver?.reInitStep = true
        }
        //create camera listener callback
        baseLoaderCallback = object : BaseLoaderCallback(App.applicationContext()) {
            override fun onManagerConnected(status: Int) {
                when (status) {
                    LoaderCallbackInterface.SUCCESS -> {
                        cameraViewer?.enableView()
                    }
                    else -> super.onManagerConnected(status)
                }
            }
        }

        buttonTrackingType.setOnClickListener {
            SheetMenu().apply {
                titleId = R.string.tracking_algorithms
                click = MenuItem.OnMenuItemClickListener {
                    cameraObserver?.apply {
                        when (it.itemId) {
                            R.id.mosse -> trackerType = TrackerType.MOSSE
                            R.id.kcf -> trackerType = TrackerType.KCF
                            R.id.boosting -> trackerType = TrackerType.BOOSTING
                            R.id.goturn -> trackerType = TrackerType.GOTURN
                            R.id.medianflow -> trackerType = TrackerType.MEDIANFLOW
                            R.id.mil -> trackerType = TrackerType.MIL
                            R.id.tld -> trackerType = TrackerType.TLD
                        }
                    }
                    return@OnMenuItemClickListener true
                }
                menu = R.menu.menu_tracking
                showIcons = false // true, by default
            }.show(activity as MainActivity)
        }
    }


    override fun onResume() {
        super.onResume()
        if (!OpenCVLoader.initDebug()) {
            // Toast.makeText(activity, "There is a problem", Toast.LENGTH_SHORT).show()
        } else {
            baseLoaderCallback?.onManagerConnected(BaseLoaderCallback.SUCCESS)
        }
    }

    override fun onPause() {
        super.onPause()
        cameraViewer?.disableView()
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        //empty
    }

    override fun onCameraViewStopped() {
        //empty
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame): Mat? {
        fps = System.currentTimeMillis()
        mat = cameraObserver?.onCameraFrame(inputFrame)
        fps = 1000 / (System.currentTimeMillis() - fps)
        Log.d("FpsSpeed", fps.toString());
        return mat
    }
}
