package com.siemens.ar.computerVision

import org.opencv.android.CameraBridgeViewBase
import org.opencv.core.Mat

interface CameraWorkDelegate {

    fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame): Mat
}
