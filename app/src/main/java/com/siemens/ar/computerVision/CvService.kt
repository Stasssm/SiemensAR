package com.siemens.ar.computerVision

import com.siemens.ar.data.model.SensorInfo
import com.siemens.ar.data.model.Tag
import io.reactivex.Observable
import io.reactivex.Single
import org.opencv.core.Mat
import org.opencv.core.Rect

interface CvService {

    fun getTag(mat : Mat, rect: Rect) : Single<String>

    fun getSensorInfoFlow() : Observable<SensorInfo>

    fun startTagListening(tag: Tag)
}