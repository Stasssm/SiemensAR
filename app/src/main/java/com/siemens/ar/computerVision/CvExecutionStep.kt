package com.siemens.ar.computerVision

import org.opencv.core.Mat

interface CvExecutionStep<T> {

    fun perform(cameraDetectionTracking: CameraDetectionTracking, dst : Mat, gray: Mat) : T?
}




