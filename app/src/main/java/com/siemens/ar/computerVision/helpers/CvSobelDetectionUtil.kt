package com.siemens.ar.computerVision.helpers

import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.opencv.tracking.Tracker

class CvSobelDetectionUtil {

    private var cannyMat: Mat = Mat()
    private var lrrIMG: Mat = Mat()

    private fun sobelDetection(gray: Mat, dst: Mat): Mat? {
        Imgproc.blur(gray, gray, Size(5.0, 5.0))
//        Imgproc.pyrDown(gray, pyrDownMat, Size((gray.cols() / 2).toDouble(), (gray.rows() / 2).toDouble()))
//        Imgproc.pyrUp(pyrDownMat, pyrUpMat, gray.size())
//        Imgproc.Canny(pyrUpMat, cannyMat, minThreshold.toDouble(), maxThreshold.toDouble())
        Imgproc.Sobel(gray, cannyMat, Core.CV_8U, 1, 0, 3, 1.0, 0.0)
        //Imgproc.Sobel(gray, lrrIMG, CV_8U, 0, 1, 3, 1.0, 0.0)
        //addWeighted( cannyMat, 0.5, lrrIMG, 0.5, 0.0, gray );

        Imgproc.threshold(cannyMat, lrrIMG, 0.0, 255.0, Imgproc.CV_THRESH_OTSU)
        val rectMorph = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, Size(17.0, 3.0))
        Imgproc.morphologyEx(lrrIMG, lrrIMG, Imgproc.CV_MOP_CLOSE, rectMorph)
//        val contours = ArrayList<MatOfPoint>()
//        findContours(lrrIMG, contours, hierarchyMat, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE)
//        val rectangles = ArrayList<RotatedRect>(0)
//        for (contour in contours) {
//            val rect = minAreaRect(MatOfPoint2f(*contour.toArray()))
//            if(!checkSizes(rect)) {
//                rectangles.add(rect)
//            }
//        }
//        for (rect in rectangles) {
//            setContour(gray, rect)
//        }
        return lrrIMG
    }

}
