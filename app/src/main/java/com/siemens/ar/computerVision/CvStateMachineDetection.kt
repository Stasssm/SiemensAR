package com.siemens.ar.computerVision

import com.siemens.ar.computerVision.util.setContour
import com.siemens.ar.data.model.Tag
import org.opencv.core.*
import org.opencv.imgproc.Imgproc

sealed class CvStateMachineDetection : CvExecutionStep<CvStateMachineDetection> {


    object FramePrepareCropping : CvStateMachineDetection() {
        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst : Mat, gray: Mat) : CvStateMachineDetection? {
            cameraDetectionTracking.apply {
                if (cameraWidth == 0) {
                    return null
                }
                if (offsetX == 0 && offsetY == 0) {
                    val width = gray.width()
                    windowWidth = (width * searchWidth) / cameraWidth
                    offsetX = (width - windowWidth) / 2
                    val height = gray.height()
                    windowHeight = (height * searchHeight) / cameraHeight
                    offsetY = (height - windowHeight) / 2
                    minSearchArea = (windowWidth * windowHeight * CameraDetectionTracking.SEARCH_FACTOR).toInt()
                }
            }
            return Cropping
        }
    }

    object Cropping : CvStateMachineDetection() {
        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst: Mat, gray: Mat): CvStateMachineDetection? {
            cameraDetectionTracking.apply {
                val grayCropped = Mat(gray, Rect(offsetX, offsetY, windowWidth, windowHeight))
                Imgproc.pyrDown(grayCropped, pyrDownMat, Size((grayCropped.cols() / 2).toDouble(), (grayCropped.rows() / 2).toDouble()))
                Imgproc.pyrUp(pyrDownMat, pyrUpMat, grayCropped.size())
            }
            return EdgeDetection
        }
    }

    object EdgeDetection : CvStateMachineDetection() {
        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst: Mat, gray: Mat): CvStateMachineDetection? {
            cameraDetectionTracking.apply {
                Imgproc.Canny(pyrUpMat, cannyMat, minThreshold.toDouble(), maxThreshold.toDouble())
                Imgproc.dilate(cannyMat, cannyMat, Mat(), Point(-1.0, 1.0), 1)
            }
            return ContourDetection
        }
    }

    object ContourDetection : CvStateMachineDetection() {
        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst: Mat, gray: Mat): CvStateMachineDetection? {
            cameraDetectionTracking.apply {
                contours = ArrayList()
                cloneCannyMat = cannyMat.clone()
                Imgproc.findContours(cloneCannyMat, contours, hierarchyMat, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE)
            }
            return ObjectDetection
        }
    }


    object ObjectDetection : CvStateMachineDetection() {

        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst: Mat, gray: Mat): CvStateMachineDetection? {
            cameraDetectionTracking.apply {
                for (cnt in contours) {
                    val curve = MatOfPoint2f(*cnt.toArray())
                    Imgproc.approxPolyDP(curve, approxCurve, 0.02 * Imgproc.arcLength(curve, true), true)
                    val numberVertices = approxCurve.total().toInt()
                    val contourArea = Imgproc.contourArea(cnt)
                    if (Math.abs(contourArea) < minSearchArea) {
                        continue
                    }
                    //Rectangle detected
                    if (numberVertices in 4..6) {
                        val cos = ArrayList<Double>()
                        for (j in 2 until numberVertices + 1) {
                            cos.add(angle(approxCurve.toArray()[j % numberVertices], approxCurve.toArray()[j - 2], approxCurve.toArray()[j - 1]))
                        }
                        cos.sort()
                        val mincos = cos[0]
                        val maxcos = cos[cos.size - 1]

                        if (numberVertices == 4 && mincos >= -0.1 && maxcos <= 0.3) {
                            curve.setContour(dst, offsetX, offsetY)
                            val innerRect = Imgproc.boundingRect(cnt)
                            rect = Rect(innerRect.x + offsetX,
                                    innerRect.y + offsetY,
                                    innerRect.width,
                                    innerRect.height)
                            tracingRect = Rect2d(innerRect.x.toDouble() + offsetX,
                                    innerRect.y.toDouble() + offsetY,
                                    innerRect.width.toDouble(),
                                    innerRect.height.toDouble())
                            return DetectionFinished
                        }
                    }
                }
            }
            return NotDetected
        }

        private fun angle(pt1: Point, pt2: Point, pt0: Point): Double {
            val dx1 = pt1.x - pt0.x
            val dy1 = pt1.y - pt0.y
            val dx2 = pt2.x - pt0.x
            val dy2 = pt2.y - pt0.y
            return (dx1 * dx2 + dy1 * dy2) / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10)
        }
    }

    object DetectionFinished : CvStateMachineDetection() {
        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst: Mat, gray: Mat): CvStateMachineDetection? {
            cameraDetectionTracking.apply {
                cvService.getTag(dst, rect ?: return null)
                        .subscribe({
                            startGettingSensorInfo(Tag(text = it))
                        }, {
                            reInitStep = true
                            it.printStackTrace()
                        })
            }
            return null
        }
    }

    object NotDetected : CvStateMachineDetection() {
        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst: Mat, gray: Mat): CvStateMachineDetection? {
            return null
        }
    }

    abstract override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst : Mat, gray: Mat) : CvStateMachineDetection?
}