package com.siemens.ar.computerVision.helpers

import android.util.Log
import org.opencv.core.*
import org.opencv.imgproc.Imgproc


class CvCompareSobelCanny {


    private var cannyMat: Mat = Mat()
    private var lrrIMG: Mat = Mat()
    private var pyrDownMat: Mat = Mat()
    private var pyrUpMat: Mat = Mat()
    private var minThreshold = 0
    private var maxThreshold = 100

    private fun compareSobelCanny(gray: Mat): Mat? {
        Imgproc.pyrDown(gray, pyrDownMat, Size((gray.cols() / 2).toDouble(), (gray.rows() / 2).toDouble()))
        Imgproc.pyrUp(pyrDownMat, pyrUpMat, gray.size())
        val timeMillis = System.currentTimeMillis()
        Imgproc.Canny(pyrUpMat, lrrIMG, minThreshold.toDouble(), maxThreshold.toDouble())
        val timeMillisCanny = System.currentTimeMillis()
        Imgproc.Sobel(pyrUpMat, cannyMat, Core.CV_8U, 1, 0, 3, 1.0, 0.0)
        val timeMillisSobel = System.currentTimeMillis()
        Log.d("SobelCanny", "Speed Canny = ${timeMillisCanny - timeMillis}")
        Log.d("SobelCanny", "Speed Sobel = ${timeMillisSobel - timeMillisCanny}")
        Log.d("SobelCannyCompare", "Speed compare = ${(timeMillisCanny - timeMillis) - (timeMillisSobel - timeMillisCanny)}")
        Imgproc.dilate(cannyMat, cannyMat, Mat(), Point(-1.0, 1.0), 1)
        return cannyMat
    }
}
