package com.siemens.ar.computerVision

import com.siemens.ar.computerVision.util.setContour
import org.opencv.core.Mat
import org.opencv.tracking.*

sealed class CvStateMachineTracking : CvExecutionStep<CvStateMachineTracking> {


    object InitTracker : CvStateMachineTracking() {
        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst: Mat, gray: Mat): CvStateMachineTracking? {
            cameraDetectionTracking.apply {
                tracker = when (trackerType) {
                    TrackerType.MOSSE -> TrackerMOSSE.create()
                    TrackerType.MIL -> TrackerMIL.create()
                    TrackerType.MEDIANFLOW -> TrackerMedianFlow.create()
                    TrackerType.KCF -> TrackerKCF.create()
                    TrackerType.BOOSTING -> TrackerBoosting.create()
                    TrackerType.TLD -> TrackerTLD.create()
                    TrackerType.GOTURN -> TrackerGOTURN.create()
                }
                tracker?.init(gray, tracingRect)
            }
            return Track
        }
    }

    object Track : CvStateMachineTracking() {
        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst: Mat, gray: Mat): CvStateMachineTracking? {
            cameraDetectionTracking.apply {
                tracker?.update(gray, tracingRect)
                tracingRect?.setContour(dst)
                sendRectToDraw(tracingRect ?: return@apply)
            }
            return TrackingFinished
        }
    }

    object TrackingFinished : CvStateMachineTracking() {
        override fun perform(cameraDetectionTracking: CameraDetectionTracking, dst: Mat, gray: Mat): CvStateMachineTracking? {
            return Track
        }
    }
}