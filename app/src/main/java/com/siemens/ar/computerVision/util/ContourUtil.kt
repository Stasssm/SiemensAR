package com.siemens.ar.computerVision.util

import org.opencv.core.*
import org.opencv.imgproc.Imgproc

fun MatOfPoint2f.setContour(im: Mat, offsetX: Int, offsetY: Int) {
    val rotatedRect = Imgproc.minAreaRect(this);
    val points = Array<Point?>(4) { null };
    rotatedRect.points(points)
    points.forEach {
        if (it != null) {
            it.x += offsetX
            it.y += offsetY
        }
    }
    val boxContours = List(1) { MatOfPoint(*points) }
    // draw enclosing rectangle (all same color, but you could use variable i to make them unique)
    Imgproc.drawContours(im, boxContours, 0,
            Scalar(255.0, 0.0, 0.0, 255.0), 5)
}

fun Rect2d.setContour(im: Mat) {
    Imgproc.rectangle(im, tl(), br(),
            Scalar(255.0, 0.0, 0.0, 255.0), 5)
}

fun RotatedRect.setContour(im: Mat) {
    val points = Array<Point?>(4) { null };
    points(points)
    val boxContours = List(1) { MatOfPoint(*points) }
    Imgproc.drawContours(im, boxContours, 0,
            Scalar(255.0, 0.0, 0.0, 255.0), 5)
}