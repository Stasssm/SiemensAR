package com.siemens.ar.computerVision

import android.annotation.SuppressLint
import org.opencv.android.CameraBridgeViewBase
import org.opencv.core.*
import org.opencv.tracking.Tracker
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.siemens.ar.data.model.SensorInfo
import com.siemens.ar.data.model.Tag
import io.reactivex.disposables.Disposable


class CameraDetectionTracking(internal var cameraWidth: Int,
                              internal var cameraHeight: Int,
                              internal var searchWidth: Int,
                              internal var searchHeight: Int,
                              var cvService: CvService) : ViewModel(), CameraWorkDelegate {

    internal var cannyMat: Mat = Mat()
    internal var pyrDownMat: Mat = Mat()
    internal var pyrUpMat: Mat = Mat()
    internal var cloneCannyMat: Mat = Mat()
    internal var hierarchyMat: Mat = Mat()
    internal var approxCurve: MatOfPoint2f = MatOfPoint2f()
    internal var minThreshold = 0
    internal var maxThreshold = 100
    internal var offsetX = 0
    internal var offsetY = 0
    internal var minSearchArea = 0;
    internal var windowHeight = 0
    internal var windowWidth = 0
    internal var tracingRect: Rect2d? = null
    internal var tracker: Tracker? = null
    internal var contours = ArrayList<MatOfPoint>()
    internal var startExecutionStep: CvExecutionStep<*>? = CvStateMachineDetection.FramePrepareCropping
    internal var reInitStep = false
    var rect: Rect? = null
    internal var trackerType = TrackerType.KCF
            set(value) {
                field = value
                reInitStep = true
            }

    private val disposable : Disposable
    private val selected = MutableLiveData<String>()
    @Volatile
    private var rectToDraw : Rect2d? =null
    private val sensorInfo = MutableLiveData<Pair<SensorInfo, Rect2d>>()

    init {
        disposable = cvService.getSensorInfoFlow().subscribe({
            sensorInfo.postValue(Pair(it, rectToDraw ?: return@subscribe))
        }, {
            it.printStackTrace()
        })
    }

    fun startGettingSensorInfo(tag: Tag) {
        cvService.startTagListening(tag)
    }

    fun getSensorInfo(): LiveData<Pair<SensorInfo, Rect2d>> {
        return sensorInfo
    }

    fun sendRectToDraw(item: Rect2d) {
        rectToDraw = item
    }

    fun addTag(item: String) {
        selected.postValue(item)
    }

    fun getTag(): LiveData<String> {
        return selected
    }

    companion object {
        internal const val SEARCH_FACTOR = 0.04;
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame): Mat {
        val gray = inputFrame.gray()
        val dst = inputFrame.rgba()
        if (reInitStep) {
            startExecutionStep = CvStateMachineDetection.FramePrepareCropping
            reInitStep = false
        }
        while (startExecutionStep != null) {
            if (startExecutionStep == CvStateMachineTracking.InitTracker) {
                performStep(gray, dst)
                return dst
            }
            performStep(gray, dst)
            if (startExecutionStep == CvStateMachineDetection.DetectionFinished) {
                performStep(gray, dst)
                startExecutionStep = CvStateMachineTracking.InitTracker
            }
            if (startExecutionStep == CvStateMachineDetection.NotDetected) {
                startExecutionStep = CvStateMachineDetection.FramePrepareCropping
                return dst
            }
            if (startExecutionStep == CvStateMachineTracking.TrackingFinished) {
                performStep(gray, dst)
                return dst
            }
        }

        return dst
    }

    private fun performStep(gray: Mat, dst: Mat) {
        startExecutionStep = startExecutionStep?.perform(this, dst, gray) as? CvExecutionStep<*>
    }

    private fun checkSizes(candidate: RotatedRect): Boolean {
        val error = 0.4
        val aspectRatio = 4.0
        val min = 15 * aspectRatio * 15
        val max = 125 * aspectRatio * 125
        val rmin = aspectRatio - aspectRatio * error
        val rmax = aspectRatio + aspectRatio * error
        val area = candidate.size.height * candidate.size.width
        var r = candidate.size.width.toFloat() / candidate.size.height.toFloat()
        if (r < 1) {
            r = 1 / r
        }
        return !((area < min || area > max) || (r < rmin || r > rmax))
    }
}

enum class TrackerType {
    MOSSE,
    MIL,
    MEDIANFLOW,
    KCF,
    BOOSTING,
    TLD,
    GOTURN
}